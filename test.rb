#!/usr/bin/env ruby

require "./lib/parser"

unless ARGV.length == 1 then
  puts "I need the name of a G-code file"
  exit
end

unless File.exist? ARGV[0] then
  puts "File #{ARGV[0]} does not exist!"
  exit
end

cfg = {
  :file_name => ARGV[0],
  :A => 10000,
  :D => 10000,
  :tq => 0.01
}

parser = RNC::Parser.new cfg
parser.parse_file

parser.blocks.each do |b|
  p b
end

parser.each_block do |block, intp|
  puts "starting block #{block.inspect}"
  intp.each_timestep do |t, cmd|
    puts [t, cmd] * " "
  end
end





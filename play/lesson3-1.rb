#!/usr/bin/env ruby

# Writing to files

File.open("test.txt", "w") do |output_file|
  10.times do |i|
    output_file.puts "#{i}, #{i**2}"
  end
end

# Reading a file, one line at a time

list_of_values = []
File.foreach("test.txt") do |line|
  a = line.chomp.split(",")
  a.map! {|e| e.to_f}
  list_of_values << a
end

p list_of_values

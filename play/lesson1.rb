#!/usr/bin/env ruby
# RUBY lesson #1

s = "Hello, World!"
puts s

a = 123.4
b = 2
a = 2

puts a * b

# String INTERPOLATION
puts "The value of 2a is #{ 2 * a }"

# single-quoted strings do not interpolate!
puts 'The value of 2a is #{ 2 * a }'

message = "..."
$a = 10 # This is a global variable, visible anywhere in our code

# FUNCTIONS
def greet(name, message="Hello")
  message = "#{message}, #{name}!"
  puts message
end

# Use of default arguments
greet("Paolo")             # using default for second arg
greet "Paolo", "G'night"   # explicit value to second arg

# CONSTANTS HAVE NAMES THAT BEGIN WITH A CAPITAL LETTER
PI = 3.14159265351

puts PI

def area(radius)
  if radius >= 0 then
    # CONSTANTS are visible within inner scopes
    return 2 * PI * radius 
  else
    return(0.0)
  end
end

puts area(10)

# OBJECTS
# Class definition
class Chalk
  attr_accessor :color  # :something is a Symbol
  
  def initialize(color="white")
    @length = 10
    @color  = color
  end
  
  # Accessor method (reading, called 'getter')
  def length
    puts "using getter custom"
    return @length
  end
  attr_accessor :length
  
  # Accessor method (writing, called 'setter')
  def length=(value)
    @length = value
  end

  def write(string)
    if @length < string.length then
      puts "..."
      @length = 0
    else 
      puts "in #{@color}: #{string}"
      @length -= string.length
    end
  end
  
  def inspect
    "I am a #{@color} Chalk of length #{@length}"
  end
end # class Chalk

ch1 = Chalk.new
ch1.write "Hello"
puts ch1.length

ch2 = Chalk.new("red")
ch2.length = 25
ch2.write "Hello, World!"
ch2.color = "blue"
ch2.write "new color"

p ch1.methods
p ch1









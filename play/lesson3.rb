#!/usr/bin/env ruby

class Test
  @@args = {
    a: 0,
    name: "My Name"
  }
  @@count = 0
  # A getter method for the class variable @@args
  def self.default_args; @@args; end
  # A setter method for the class variable @@args
  def self.default_args=(v); @@args = v; end
  def self.how_many; @@count; end
  
  attr_accessor :args
  def initialize(args={})
   
    @@count += 1
    unless args.kind_of?(Hash) then
      warn "I need a Hash, and got #{args.class}"
      args = {}
    end
    @args = @@args.merge args
    puts "I got #{args.inspect}"
  end
  
  def method_1(a, *args)
    puts "Got #{a}, #{args}"
  end

end #class Test


t1 = Test.new
p t1.args

puts "*" * 20

Test.default_args = {b:0, str:"a string", a:[0,0,0]}

t2 = Test.new a:[1,2,3]
p t2.args

puts "*" * 20

t3 = Test.new 10
p t3.args

puts "Default Test attributes are #{Test.default_args}"

puts "#{Test.how_many} instances of Test class have been created"

puts "=" * 20

t1.method_1 "mandatory arg"
t1.method_1("mandatory arg", 1)
t1.method_1(1, [1,2,3], {a:1})
t1.method_1


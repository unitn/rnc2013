#!/usr/bin/env ruby

#class Fixnum
#  alias :plus :+
#  
#  def +(v)
#     self.plus(v).plus(1)
#  end
#  
#  def greet
#    puts "Hello, I am #{self}!"
#  end
#end


# CONTAINERS (Array and Hash classes)
# Range
rng = 1..10
a = rng.to_a

# Array examples:
puts a[0]
puts a.last
puts a.first
puts a[-2]
puts a.inspect
p a # equals to puts a.inspect
puts 

a.each_with_index do |e, i|
  puts "a[#{i}] = #{e}"
end

s = a.inject(1) do |product, value|
  product * value
end

puts s

p a[3..6]
b = ["string", :symbol, 1, 1.0, [1, 2, 3]]
p b[-1][0]

10.times do |i|
  puts i
end

1.upto(10) do |i|
  puts i
end

for i in 10..20 # NOT A BLOCK!!!
  puts i
end

(0..100).step(3) do |i|
  puts i
end

# Hash
me = {:name => "Paolo", :surname => "Bosetti"}
me = {name: "Paolo", surname: "Bosetti", age: 15}
puts me["name"]

me[:height] = 180
p me













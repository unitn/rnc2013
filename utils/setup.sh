#!/bin/bash 
RUBY_BASE=ruby-2.0.0-p247

echo 
echo "************************************"
echo "Updating package list"
if ! sudo apt-get update; then
  echo "Could not update package list"
  exit $?
fi
 
pkgs=(git clang make gnuplot octave curl libreadline6-dev libssl-dev zlib1g-dev libglew-dev libglu1-mesa-dev freeglut3-dev ntp)

echo 
echo "************************************"
echo "Installing packages"
for p in ${pkgs[@]}
do
  echo
  echo "Installing ${p}"
  echo "---------------"
  if ! sudo apt-get install -y --force-yes ${p}; then
    echo "Error while installing ${p}"
    exit $?
  fi
done

echo 
echo "************************************"
if command -v ruby >/dev/null; then
  echo "Ruby already installed"
else
  echo "Installing $RUBY_BASE"
  wget http://cache.ruby-lang.org/pub/ruby/2.0/$RUBY_BASE.tar.gz
  tar xzf $RUBY_BASE.tar.gz
  cd $RUBY_BASE
  ./configure --prefix=/usr/local --with-search-path=/usr
  make
  sudo make install
  cd ..
  rm -rf $RUBY_BASE*
fi
echo 
echo "************************************"
echo "Installing ruby gems"
sudo gem install pry colorize ffi gnuplotr --no-rdoc --no-ri
 
echo 
echo "************************************"
echo " ALL DONE!"
echo "************************************"
echo

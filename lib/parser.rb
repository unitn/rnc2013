#!/usr/bin/env ruby

module RNC
  AXES = {:X => 0, :Y => 1, :Z => 2}
  COMMANDS = [:G00, :G01]
  
  class Point < Array
  
    def self.[](x=nil, y=nil, z=nil)
      self.new([x,y,z])
    end
    
    def [](index)
      super(index_remap(index))
    end
    
    def []=(index, value)
      super(index_remap(index), value)
    end
    
    def -(other)
      Math::sqrt(
        (self[0] - other[0]) ** 2 +
        (self[1] - other[1]) ** 2 +
        (self[2] - other[2]) ** 2
      )
    end
    
    def delta(other)
      Point[
        self[0] - other[0],
        self[1] - other[1],
        self[2] - other[2]
      ]
    end
    
    def modal!(other)
      [:X, :Y, :Z].each do |k|
        self[k] = other[k] unless self[k]
      end
    end
    
    def inspect
      return "[#{self[0] or '*'}, #{self[1] or '*'}, #{self[2] or '*'}]"
    end
    
    private
    def index_remap(index)
      case index
      when Numeric
        index = index.to_i
      when String
        index = AXES[index.upcase.to_sym]
      when Symbol
        index = AXES[index]
      else
        raise ArgumentError
      end
      return index
    end
    
    
  end # class Point


  class Block
    attr_reader :start, :target, :feed
    attr_reader :delta, :spindle_rate
    attr_reader :type, :length
    attr_accessor :profile, :dt
    def initialize(line = "G00 X0 Y0 Z0 F1000")
      @target = Point[nil, nil, nil]
      @feed   = nil
      @spindle_rate = nil
      @type         = nil
      @profile      = nil
      @dt           = nil
      @length       = nil
      @delta        = nil
      self.line     = line
    end
  
    def line=(l)
      @line = l.upcase
      self.parse
    end
    
    def parse
      # "one two three".split => ['one', 'two', 'three']
      tokens = @line.split
      @type = tokens.shift.to_sym
      raise RuntimeError, "Unimplemented command #{@type}" unless  COMMANDS.include? @type
      
      # iterate over remaining tokens elements
      tokens.each do |t|
        # t is something like "X123.2"
        cmd, arg = t[0], t[1..-1].to_f
        case cmd
        when "X", "Y", "Z"
          @target[cmd] = arg
        when "S"
          @spindle_rate = arg
        when "F"
          @feed = arg
        else
          raise RuntimeError, "Unknown command #{cmd}"
        end
      end     
    end
    
    # G00 X100 Y0 Z100 S5000
    # G01 X0 F1000
    # G02 Y50
    def modal!(previous)
      raise ArgumentError, "Need a block!" unless previous.kind_of? Block
      @start  = previous.target
      @target.modal!(@start)
      @feed   = (@feed or previous.feed)
      @spindle_rate = (@spindle_rate or previous.spindle_rate)
      @length = @target - @start
      @delta  = @target.delta(@start)
      return self
    end
    
    def inspect
      "[#{@type} #{@target.inspect} F:#{@feed} S:#{@spindle_rate}]"
    end
    
  end # class Block
  
  
  class Parser
    attr_reader :blocks, :file_name
    def initialize(cfg)
      @cfg = cfg
      @blocks = [Block.new]
      @file_name = cfg[:file_name]
      @profiler  = Profiler.new(cfg)
    end
    
    def parse_file
      puts ">> Parsing file #{@file_name}"
      File.foreach(@file_name) do |line|
        puts "   parsing block #{line}"
        next if line.size <= 1
        b = Block.new(line).modal!(@blocks.last)
        b.profile = @profiler.velocity_profile(b.feed, b.length)
        b.dt = @profiler.dt
        @blocks << b
      end
    end
    
    def each_block
      interp = Interpolator.new(@cfg)
      @blocks.each do |block|
        interp.block = block
        yield block, interp
      end
    end
    
  end # class Parser
  
  
  class Profiler
    attr_reader :dt, :accel, :feed, :times
    def initialize(cfg)
      @cfg   = cfg # a Hash!
      @feed  = 0.0
      @dt    = 0.0
      @times = []
      @accel = []
    end
    
    def velocity_profile(f_m, l)
      f_m = f_m.to_f
      l   = l.to_f    # 1000 becomes 1000.0
      
      dt_1 = f_m / @cfg[:A]
      dt_2 = f_m / @cfg[:D]
      dt_m = l / f_m - (dt_1 + dt_2) / 2.0
      
      if dt_m > 0 then # Trapezoidal profile
        q = quantize(dt_1 + dt_2 + dt_m, @cfg[:tq])
        dt_m += q[1]
        f_m = (2 * l) / (dt_1 + dt_2 + 2 * dt_m)
      else             # Tiangular profile
        dt_1 = Math::sqrt(2 * l / ( @cfg[:A] + @cfg[:A]**2 / @cfg[:D]))
        dt_2 = dt_1 * @cfg[:A] / @cfg[:D]
        q = quantize(dt_1 + dt_2, @cfg[:tq])
        dt_m = 0.0
        dt_2 += q[1]
        f_m = 2 * l / (dt_1 + dt_2)
      end
      # updated accelerations:
      a = f_m / dt_1
      d = -(f_m / dt_2)
      
      @times = [dt_1, dt_m, dt_2]
      @feed  = f_m
      @accel = [a, d]
      @dt    = q[0]
      
      return proc do |t|
        r = 0.0
        if t < dt_1 then
          type = :A
          r = a * t ** 2 / 2.0
        elsif t < dt_1 + dt_m then
          type = :M
          r = f_m * dt_1 / 2.0 + f_m * (t - dt_1)
        else
          type = :D
          t_2 = dt_1 + dt_m
          r = f_m * dt_1 / 2.0 + f_m * (dt_m + t - t_2) + d / 2.0 * (t ** 2 + t_2 ** 2) - d * t * t_2
        end
        {:lambda => r / l, :type => type} 
      end
      
      
    end
    
    private
    def quantize(t, dt)
      result = []
      if (t % dt) == 0 then
        result = [t, 0.0]
      else      
        result[0] = ((t / dt).to_i + 1) * dt
        result[1] = result[0] - t
      end  
      return result # quantize(1.0, 0.3) => [1.2, 0.2]
    end
    
    
    
  end # class Profiler
  
  
  class Interpolator
    attr_accessor :block
    def initialize(cfg)
      @cfg = cfg
      @block = nil
    end
    
    def each_timestep(tq=@cfg[:tq])
      raise "I Need a block!" unless block_given?
      t = 0.0
      while (cmd = self.eval(t)) do
        yield t, cmd
        t += tq
      end
    end
    
    def eval(t)
      if @block.type == :G00 then # Rapid block
        return nil
      end
      if t > @block.dt + 2.0 * Float::EPSILON then # end of interpolation block
        return nil
      end
      result = {}
      case @block.type
      when :G00
        result[:position] = @block.target
        result[:lambda]   = 0.0
        result[:type]     = :R
      when :G01
        result = @block.profile.call(t)
        result[:position] = Point[]
        [:X, :Y, :Z].each do |axis|
          result[:position][axis] = @block.start[axis] + result[:lambda] * @block.delta[axis]
        end
      else
        raise RuntimeError, "Unimplemented command #{@block.type}"
      end
      return result
    end
    
  end # class Interpoltor  

end # module RNC





